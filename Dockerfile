FROM node:14.4.0-alpine3.11

ENV PATH_TO_APP=/usr/src/app

WORKDIR ${PATH_TO_APP}

COPY package.json package-lock.json ./

RUN npm install

COPY . ./

CMD npm run dev