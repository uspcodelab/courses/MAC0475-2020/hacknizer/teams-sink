import { MongoClient } from 'mongodb';

const { CACHE_URL, CACHE_DB } = process.env;

export async function setupDB() {
  const client = await MongoClient.connect(CACHE_URL);
  return client.db(CACHE_DB);
}
