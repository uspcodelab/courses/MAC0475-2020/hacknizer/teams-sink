import * as natsStreaming from 'node-nats-streaming';

export async function mockEvents(broker) {
    broker.publish('person', JSON.stringify({id: "1", name: "Ana da Silva" }));
    broker.publish('person', JSON.stringify({id: "2", name: "Bernardo di Caprio"}));
    broker.publish('person', JSON.stringify({id: "3", name: "Carlos Fonseca"}));
    broker.publish('person', JSON.stringify({id: "4", name: "Danilo do Campo"}));
    broker.publish('person', JSON.stringify({id: "5", name: "Emilia Gentili"}));
    broker.publish('person', JSON.stringify({id: "6", name: "Francisco Morato"}));
    broker.publish('person', JSON.stringify({id: "7", name: "Geraldo Fernandes"}));

}