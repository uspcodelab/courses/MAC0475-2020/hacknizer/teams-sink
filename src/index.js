import * as natsStreaming from 'node-nats-streaming';

import { setupDB } from './db';
import { mockEvents } from './mock';

const { BROKER_URL, BROKER_CLUSTER_ID } = process.env;

const stan = natsStreaming.connect(BROKER_CLUSTER_ID, 'sink-boilerplate', {
  url: BROKER_URL,
});

async function saveToDB(collection, obj, idField) {
  const query = {};
  query[idField] = obj[idField];
  const dbObj = await collection.findOne(query);
  // console.log(dbObj);

  if (!dbObj) {
    obj._id = obj[idField];
    collection.insertOne(obj);
  } else {
    collection.replaceOne(query, obj);
  }
}

stan.on('connect', async () => {
  const db = await setupDB();

  const replayAllOpt = stan.subscriptionOptions().setDeliverAllAvailable();
  const teamSubs = stan.subscribe('team', replayAllOpt);

  // setTimeout(() => teamSubs.unsubscribe(), 1500);
  // teamSubs.on('unsubscribed', () => stan.close());

  teamSubs.on('message', async (msg) =>{
    const obj = JSON.parse(msg.getData());
    saveToDB(db.collection("team"), obj, "uuid");
  });

  const personSubs = stan.subscribe('person', replayAllOpt);
  personSubs.on('message', async (msg) =>{
    const obj = JSON.parse(msg.getData());
    console.log(obj);
    saveToDB(db.collection("available_member"), obj, "id");
  });

  const memberAddedSubs = stan.subscribe('member_added', replayAllOpt);
  memberAddedSubs.on('message', async (msg) =>{
    const obj = JSON.parse(msg.getData());
    db.collection("available_member").deleteOne({id: obj.id});
  });

  // stan.publish('people', 'hello, other world!');
  // mockEvents(stan);
});

stan.on('close', () => {console.log("saindo"); process.exit();});